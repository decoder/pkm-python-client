#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

command="python test_pkm_python_client.py"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
