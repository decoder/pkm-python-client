import os
import sys

from pkm_python_client.pkm_client import PKMClient

PKM_USER = os.environ.get('PKM_USER')
PKM_PASSWORD = os.environ.get('PKM_PASSWORD')
PKM_CERT = os.environ.get('PKM_CERT')
if PKM_CERT is None:
    PKM_CERT = "/ssl/pkm_docker.crt"

if __name__ == "__main__":
    pkm = PKMClient(cacert=PKM_CERT)
    status = pkm.login(user_name=PKM_USER, user_password=PKM_PASSWORD)
    if not status:
        print(f"user's login to pkm failed", file=sys.stderr)
    else:
        print(f"succesfully logged user to pkm")
