FROM python:3.9-slim-buster

# --------------------
# Package installation
# --------------------

ARG DEBIAN_FRONTEND=noninteractive
ARG THREADS

#RUN apt-get clean && apt-get update && apt-get install -y -qq

COPY requirements.txt /
RUN pip install -r /requirements.txt
COPY ./pkm_python_client /pkm_python_client
COPY ./tests /tests
COPY ./ssl /ssl

ARG PUID
ARG GUID

# CREATE APP USER
RUN groupadd -g ${GUID} apiuser && \
useradd -r -u ${PUID} -g apiuser apiuser

RUN chown -R ${PUID}:${GUID} /pkm_python_client

# install SSL certificate of PKM server
RUN mkdir -p /usr/local/share/ca-certificates && chmod 755 /usr/local/share/ca-certificates && cp -f /ssl/pkm_docker.crt /usr/local/share/ca-certificates && chmod 644 /usr/local/share/ca-certificates/pkm_docker.crt && update-ca-certificates

ENV PYTHONPATH=/pkm_python_client

ENTRYPOINT ["python","/tests/test_pkm_python_client.py"]
